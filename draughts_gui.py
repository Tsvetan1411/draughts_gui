import tkinter as tk
from pathlib import Path
import pawns

gif_file_path = Path.cwd() / "pawns_gif"


class DraughtsBoard:
    def __init__(self, root, board, cpu, player_turn):
        self.buttons = {}
        self.root = root
        self.root.title("Draughts Board")
        self.white_dame_gif = tk.PhotoImage(file=str(gif_file_path)+"/white_dame.gif")
        self.black_dame_gif = tk.PhotoImage(file=str(gif_file_path)+"/black_dame.gif")
        self.white_pawn_gif = tk.PhotoImage(file=str(gif_file_path)+"/white_pawn.gif")
        self.black_pawn_gif = tk.PhotoImage(file=str(gif_file_path)+"/black_pawn.gif")
        self.empty_field_gif = tk.PhotoImage(file=str(gif_file_path)+"/empty_field.gif")
        self.made_choice = False
        self.player_choice = None
        self.board = board
        self.cpu = cpu
        self.player_turn = player_turn
        self.info = "Wybierz pionek"
        
        for col in range(8):
            label = tk.Label(self.root, text=chr(ord("A") + col))
            label.grid(row=0, column=col + 1)

        for row in range(8):
            label = tk.Label(self.root, text=str(1 + row))
            label.grid(row=row + 1, column=0)

        for cell in range(9):
            self.root.columnconfigure(cell, weight=1)
            self.root.rowconfigure(cell, weight=1)
        
        for row in range(8):
            for col in range(8):
                button = tk.Button(self.root, text=f"{chr(ord('a') + col)}{str(row + 1)}", width=12, height=5, 
                                   relief=tk.RAISED, bg="white", fg="white",
                                   command=lambda r=row, c=col: self.button_callback(r, c))
                button.grid(row=row + 1, column=col + 1, sticky="nsew")
                button_adres = chr(ord("a") + col) + str(row + 1)
                self.buttons[button_adres] = button

        self.info_label = tk.Label(self.root, text=self.info, height=3, bg="gray", fg="blue", relief=tk.SUNKEN)
        self.info_label.grid(row=9, columnspan=9, sticky="nsew")

        if not player_turn:
            self.board.moves_serch_initialization()
            mc_player, mc_cpu = self.board.mandatory_captures()
            pawn, move = self.cpu.moves(mc_cpu, self.board.board_dict)
            self.board.board_dict[pawn].movement(self.board.board_dict, mc_cpu, move)
            self.info = (f"Przeciwnik ruszył się z {pawn} na {move}")
            self.update_board()           

    def button_callback(self, row, col):
        field_name = f"{chr(ord('a') + col)}{1 + row}"
        self.board.moves_serch_initialization()
        mc_player, mc_cpu = self.board.mandatory_captures()
        if not self.made_choice:
            if isinstance(self.board.board_dict[field_name], pawns.Pawn) and self.board.board_dict[field_name].is_player == True:
                    if mc_player and field_name not in mc_player:
                        self.info = "Masz obowiązkowe bicia!"
                    else:
                        self.player_choice = field_name
                        self.made_choice = True
            else:
                self.info = "Błędne polecenie"
        else:
           self.board.board_dict, self.info, made_move = self.board.board_dict[self.player_choice].movement(self.board.board_dict, mc_player, field_name)
           if made_move:
                self.made_choice = False
                self.player_choice = None
                self.board.moves_serch_initialization()
                mc_player, mc_cpu = self.board.mandatory_captures()
                pawn, move = self.cpu.moves(mc_cpu, self.board.board_dict)
                self.board.board_dict[pawn].movement(self.board.board_dict, mc_cpu, move)
                self.info = (f"Przeciwnik ruszył się z {pawn} na {move}")
        self.board.change_to_dame()
        self.update_board()
        
    
    def update_board(self):
        self.info_label.config(text=self.info)
        for address, pawn in self.board.board_dict.items():
            if isinstance(pawn, pawns.Pawn):
                if pawn.dame:
                    if pawn.color == "white":
                        self.buttons[address].config(image=self.white_dame_gif)
                    else:
                        self.buttons[address].config(image=self.black_dame_gif)
                else:
                    if pawn.color == "white":
                        self.buttons[address].config(image=self.white_pawn_gif)
                    else:
                        self.buttons[address].config(image=self.black_pawn_gif)
            else:
                self.buttons[address].config(image="")
                
            