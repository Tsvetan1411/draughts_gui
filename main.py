import pawns
import board
import opponent
import tkinter as tk
import draughts_gui as dg
from random import randint


colors = ["white", "black"]
player_color = colors[randint(0, 1)]
cpu_color = colors[0] if player_color == colors[1] else colors[1]
cpu = opponent.Opponent(cpu_color)
new_board = board.Board()
new_board.board_dict = pawns.setup_random_pawns(new_board.board_dict, player_color)
player_turn = True if player_color == "white" else False

root = tk.Tk()
gui_board = dg.DraughtsBoard(root, new_board, cpu, player_turn)
gui_board.update_board()
root.mainloop()