from random import randint
from pawns import Pawn

class Opponent:
    def __init__(self, color):
        self.color = color

    def moves(self, mandatory_captures, board_dict):
        move = None
        pawn = None
        if mandatory_captures:
            pawn = mandatory_captures[randint(0, len(mandatory_captures) - 1)]
            av_captures = list(board_dict[pawn].moves["av_captures"].keys())
            random_capture = av_captures[randint(0, len(av_captures) - 1)]
            av_capture_moves = board_dict[pawn].moves["av_captures"][random_capture]
            move = av_capture_moves[randint(0, len(av_capture_moves) - 1)]
        else:
            av_pawns = []
            for field in board_dict.keys():
                if isinstance(board_dict[field], Pawn) and board_dict[field].color == self.color:
                    if board_dict[field].moves["av_moves"]:
                       av_pawns.append(field)
            pawn = av_pawns[randint(0, len(av_pawns) - 1)]
            av_moves = board_dict[pawn].moves["av_moves"]
            move = av_moves[randint(0, len(av_moves) - 1)]
        return pawn, move

